#include <stdio.h>
#ifndef PKG_VER
#define PKG_VER "NO_VER"
#endif

int main() {
    printf("Hello from version %s!\n", PKG_VER);
    return 0;
}
